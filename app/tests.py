import unittest
from app import create_app


class FlaskTestCase(unittest.TestCase):

    def test_when_health_check_is_called_then_it_responds(self):
        tester = create_app().test_client(self)
        response = tester.get('/healthcheck', content_type='application/json')
        self.assertEqual(response.status_code, 200)

    def test_when_login_page_is_requested_then_login_page_shows(self):
        tester = create_app().test_client(self)
        response = tester.get('/login', content_type='html/text')
        print(response)
        self.assertIn(b'<label>Username</label>', response.data)


if __name__ == '__main__':
    unittest.main()
