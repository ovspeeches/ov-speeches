import os

from flask_login import UserMixin

from app import login_manager


class User(UserMixin):
    def __init__(self, id):
        self.id = id
        self.name = os.getenv('APP_USERNAME')
        self.password = os.getenv('APP_PASSWORD')

    def __repr__(self):
        return '<User {}>'.format(self.name)


@login_manager.user_loader
def load_user(user_id):
    return User(user_id)


admin = User(1)
