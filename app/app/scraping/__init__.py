from flask import Blueprint

blueprint = Blueprint('scraping', __name__)

from app.scraping import routes