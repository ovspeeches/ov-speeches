def process_paragraphs(current_page, scraping_type):
    raw_paragraphs = current_page.select('div.article_content > div.clr')[0]

    interviewer_question_tags = ['b', 'i', 'em']
    interviewer_questions = []

    for node in raw_paragraphs.find_all(True):

        is_innermost_element = len(node.contents) > 0 and node.text and node.contents[0] == node.text

        if is_innermost_element:

            if node.name in interviewer_question_tags:
                interviewer_questions.append(node.text.strip())

    processed_paragraphs = []

    for node in raw_paragraphs.stripped_strings:
        if node.strip() == 'miniszterelnok.hu' or node.strip() == '' or node.strip() == 'orbanviktor.hu':
            continue

        processed_paragraphs.append({
            "content": node,
            "interviewer_question": scraping_type == 'interjuk' and node in interviewer_questions
        })

    return processed_paragraphs


