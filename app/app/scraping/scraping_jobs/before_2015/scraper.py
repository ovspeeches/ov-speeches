import logging
import requests
from bs4 import BeautifulSoup
import app.scraping.scraping_jobs.before_2015
from app.scraping.scraping_jobs.elastic_client import put_interview, put_speech

try:
    from urllib.parse import urlparse
except ImportError:
    from urlparse import urlparse
from datetime import datetime

border_date = datetime(2015, 11, 19).timestamp()

logger = logging.getLogger(__name__)

DOMAIN = 'http://2010-2015.miniszterelnok.hu'
month_mapping = {
    "január": "january",
    "február": "february",
    "március": "march",
    "április": "april",
    "május": "may",
    "június": "june",
    "július": "july",
    "augusztus": "august",
    "szeptember": "september",
    "október": "october",
    "november": "november",
    "december": "december",
}


def get_number_of_listing_pages(scraping_type):
    main_pages_response = requests.get("{}/{}".format(DOMAIN, scraping_type))
    soup = BeautifulSoup(main_pages_response.content, 'html.parser')
    return int(soup.select("div.pager")[0].select('a')[-1]['href'].split('/')[-2])


def get_links_of_individual_speeches(scraping_type, number_of_listing_pages):
    links = []

    for page in range(0, number_of_listing_pages + 1, 10):
        parent_page_url = "{}/{}/{}/".format(DOMAIN, scraping_type, str(page))
        current_parent_page = BeautifulSoup(requests.get(parent_page_url).content, 'html.parser')
        logger.info("Successfully scraped parent page {}".format(parent_page_url))

        links_in_parent_page = current_parent_page.select("#content .news_list div.row a")
        dates_in_parent_page = current_parent_page.select("#content .news_list div.row div.date")

        for link, date in zip(links_in_parent_page, dates_in_parent_page):

            date = date.text.strip()
            for mapping_from, mapping_to in month_mapping.items():
                date = date.replace(mapping_from, mapping_to)

            try:
                date = datetime.strptime(date, '%Y. %B %d.').timestamp()
            except ValueError:
                date = links[-1].get("date")

            links.append({
                "link": link['href'],
                "date": date
            })

    return links


def extract_speech_content(link, date, scraping_type):
    response = requests.get(DOMAIN + link)
    logger.info("_______________________________________________")
    logger.info("Status {} for link {}".format(response.status_code, DOMAIN + link))

    current_page = BeautifulSoup(response.content, 'html.parser', from_encoding="UTF-8")

    # todo: get the date outside of this function
    return {
        "title": current_page.find('h3', class_='title').text.strip(),
        "date": date,
        "lead": current_page.find('p', class_='lead').text.strip(),
        "uid": urlparse(link).path.replace("/", ""),
        "paragraphs": app.scraping.scraping_jobs.before_2015.process_paragraphs(current_page, scraping_type),
        "url": DOMAIN + link
    }


def scrape(scraping_type):
    logger.info("Started scraping type {}".format(scraping_type))

    number_of_listing_pages = get_number_of_listing_pages(scraping_type)

    logger.info("Number of listing pages to be scraped is {}".format(number_of_listing_pages))

    links_with_dates = get_links_of_individual_speeches(scraping_type, number_of_listing_pages)

    logger.info("Scraped {} individual links to extract speeches from".format(len(links_with_dates)))

    for link in links_with_dates:
        speech = extract_speech_content(link.get("link"), link.get("date"), scraping_type)

        if speech.get("date") > border_date:
            continue

        if scraping_type == 'beszedek':
            response = put_speech(speech)
        else:
            response = put_interview(speech)

        if response.status_code == 409:
            logger.info("Speech is already indexed into elasticsearch. Url: {}".format(speech.get("url")))
        else:
            logger.info("Speech with url {} scraped. Elasticsearch response: {}"
                        .format(speech.get("url"), response.json()))

    logger.info("Finished scraping type {}".format(scraping_type))
