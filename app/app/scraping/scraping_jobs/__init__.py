import logging

from app.scraping.scraping_jobs.current import scrape_current
from app.scraping.scraping_jobs.before_2015 import scrape
from app.scraping.scraping_jobs.current.scraper import delta_scrape

logger = logging.getLogger(__name__)

scraping_types = ['beszedek', 'interjuk']


def scrape_speeches_and_interview_between_2010_and_2015(uid):
    logger.info("Started scraping speeches and interviews between 2010 and 2015, uid: {}".format(uid))

    for scraping_type in scraping_types:
        scrape(scraping_type)


def scrape_speeches_and_interviews_after_2016(uid):
    logger.info("Started scraping speeches and interviews for the current period, uid: {}".format(uid))

    for scraping_type in scraping_types:
        scrape_current(scraping_type)


def scrape_new_speeches_and_interviews_after_2016(uid):
    logger.info("Started delta import, uid: {}".format(uid))

    for scraping_type in scraping_types:
        delta_scrape(scraping_type)
