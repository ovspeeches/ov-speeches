import logging
from datetime import datetime

from app.scraping.scraping_jobs.elastic_client import put_speech, put_interview

try:
    from urllib.parse import urlparse
except ImportError:
    from urlparse import urlparse
import requests
from bs4 import BeautifulSoup

logger = logging.getLogger(__name__)

DOMAIN = 'http://www.miniszterelnok.hu'
month_mapping = {
    "január": "january",
    "február": "february",
    "március": "march",
    "április": "april",
    "május": "may",
    "június": "june",
    "július": "july",
    "augusztus": "august",
    "szeptember": "september",
    "október": "october",
    "november": "november",
    "december": "december",
}


def get_number_of_listing_pages(scraping_type):
    result = requests.get("{}/category/{}/".format(DOMAIN, scraping_type))
    soup = BeautifulSoup(result.content, 'html.parser')

    return int(soup.select(".navigation ul li")[-2].select("a")[0].text)


def get_links_of_individual_speeches(scraping_type, number_of_listing_pages):
    links = []

    for page in range(1, number_of_listing_pages + 1):
        parent_page_ulr = "{}/category/{}/page/{}/".format(DOMAIN, scraping_type, str(page))
        current_parent_page = BeautifulSoup(requests.get(parent_page_ulr).content, 'html.parser')

        links_in_parent_page = current_parent_page.select("#category_element a")
        dates_in_parent_page = current_parent_page.select("#category_post_time")

        for link, date in zip(links_in_parent_page, dates_in_parent_page):

            date = date.text.strip()
            for mapping_from, mapping_to in month_mapping.items():
                date = date.replace(mapping_from, mapping_to)

            links.append({
                    "link": link['href'],
                    "date": datetime.strptime(date, '%Y. %B %d.').timestamp()
                })

    return links


def extract_speech_content(link, date):
    current_page = BeautifulSoup(requests.get(link).content, 'html.parser', from_encoding="UTF-8")

    paragraphs = []

    for paragraph in current_page.select("div#post_content_col_1 > p"):

        question_in_em = paragraph.select("em")
        question_in_i = paragraph.select("i")

        if len(question_in_em) > 0 or len(question_in_i) > 0:
            interviewer_question = True
        else:
            interviewer_question = False

        paragraphs.append({
            "content": paragraph.text.strip(),
            "interviewer_question": interviewer_question
        })

    return {
        "title": current_page.find('div', id="post_title").text.strip(),
        "date": date,
        "lead": current_page.find('div', id="post_lead").text.strip(),
        "uid": urlparse(link).path.replace("/", ""),
        "paragraphs": paragraphs,
        "url": link
    }


def scrape_current(scraping_type):
    logger.info("Started scraping type {}".format(scraping_type))

    number_of_listing_pages = get_number_of_listing_pages(scraping_type)

    logger.info("Number of listing pages to be scraped is {}".format(number_of_listing_pages))

    links_with_dates = get_links_of_individual_speeches(scraping_type, number_of_listing_pages)

    logger.info("Scraped {} individual links to extract speeches from".format(len(links_with_dates)))

    for link in links_with_dates:
        speech = extract_speech_content(link.get("link"), link.get("date"))

        if scraping_type == 'beszedek':
            response = put_speech(speech)
        else:
            response = put_interview(speech)

        if response.status_code == 409:
            logger.info("Speech is already indexed into elasticsearch. Url: {}".format(speech.get("url")))
        else:
            logger.info("Speech with url {} scraped. Elasticsearch response: {}"
                        .format(speech.get("url"), response.json()))

    logger.info("Finished scraping type {}".format(scraping_type))


def delta_scrape(scraping_type):
    logger.info("Started delta scraping for type {}".format(scraping_type))

    links_with_dates = get_links_of_individual_speeches(scraping_type, 1)

    for link in links_with_dates:
        speech = extract_speech_content(link.get("link"), link.get("date"))

        if scraping_type == '/beszedek':
            response = put_speech(speech)
        else:
            response = put_interview(speech)

        if response.status_code == 409:
            logger.info("Speech is already indexed into elasticsearch. Url: {}".format(speech.get("url")))
            break
        else:
            logger.info("Speech with url {} scraped. Elasticsearch response: {}"
                        .format(speech.get("url"), response.json()))

    logger.info("Finished delta scraping type {}".format(scraping_type))
