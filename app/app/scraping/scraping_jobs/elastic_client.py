import json
import requests


def put_interview(interview):
    return requests.put("http://elasticsearch:9200/interviews/_doc/{}/_create".format(interview.get("uid")),
                        data=json.dumps({
                            "title": interview.get("title"),
                            "lead": interview.get("lead"),
                            "url": interview.get("url"),
                            "date": interview.get("date"),
                            "paragraphs": [{
                                "content": paragraph.get("content"),
                                "interviewer_question": paragraph.get("interviewer_question")
                            } for paragraph in interview.get("paragraphs")]
                        }),
                        headers={'Content-type': 'application/json', 'Accept': 'application/json'}
                        )


def put_speech(speech):
    return requests.put("http://elasticsearch:9200/speeches/_doc/{}/_create".format(speech.get("uid")),
                        data=json.dumps({
                            "title": speech.get("title"),
                            "lead": speech.get("lead"),
                            "url": speech.get("url"),
                            "date": speech.get("date"),
                            "paragraphs": [{
                                "content": paragraph.get("content"),
                                "interviewer_question": paragraph.get("interviewer_question")
                            } for paragraph in speech.get("paragraphs")]
                        }),
                        headers={'Content-type': 'application/json', 'Accept': 'text/plain'}
                        )
