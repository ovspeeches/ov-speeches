import logging
import multiprocessing
import uuid

from apscheduler.schedulers.background import BackgroundScheduler
from app.scraping import blueprint
from flask_login import login_required

from app.scraping.scraping_jobs import scrape_speeches_and_interview_between_2010_and_2015, \
    scrape_speeches_and_interviews_after_2016, scrape_new_speeches_and_interviews_after_2016

logger = logging.getLogger(__name__)


@blueprint.route('/scrape')
@login_required
def scrape_current():
    uid = uuid.uuid1()
    logger.info('Uid: {0}, message: Scraping started'.format(uid))
    process = multiprocessing.Process(target=scrape_speeches_and_interviews_after_2016, args=(uid,))
    process.start()
    return '{"message": "Initiated scraping"}'


@blueprint.route("/scrape-between-2010-and-2016")
@login_required
def scrape_between_2010_and_2015():
    uid = uuid.uuid1()
    logger.info('Uid: {0}, message: Scraping started for years between 2010 and 2016'.format(uid))
    process = multiprocessing.Process(target=scrape_speeches_and_interview_between_2010_and_2015, args=(uid,))
    process.start()
    return '{"message": "Initiated scraping for years between 2010 and 2016"}'


@blueprint.route("/run-delta-scraping")
@login_required
def delta_import_endpoint():
    run_delta_import()
    return '{"message": "Initiated delta scraping"}'


def run_delta_import():
    uid = uuid.uuid1()
    logger.info("Uid: {}, message: Delta scraping started".format(uid))
    process = multiprocessing.Process(target=scrape_new_speeches_and_interviews_after_2016, args=(uid,))
    process.start()


scheduler = BackgroundScheduler(timezone="Europe/Budapest")
scheduler.add_job(run_delta_import, trigger='cron', hour='6')
scheduler.start()
