import logging

from config import Config
from flask import Flask
from flask_login import LoginManager

login_manager = LoginManager()
login_manager.login_view = 'admin.login'


def create_app(config_class=Config):
    app = Flask(__name__,
                static_url_path='/api/static')

    app.config.from_object(config_class)

    login_manager.init_app(app)

    from app.admin import blueprint as admin_blueprint
    app.register_blueprint(admin_blueprint, url_prefix='/api')

    from app.elastic_proxy import blueprint as elastic_proxy_blueprint
    app.register_blueprint(elastic_proxy_blueprint, url_prefix='/api')

    from app.errors import blueprint as errors_blueprint
    app.register_blueprint(errors_blueprint, url_prefix='/api')

    from app.scraping import blueprint as scraping_blueprint
    app.register_blueprint(scraping_blueprint, url_prefix='/api')

    logging.basicConfig(level=logging.INFO)

    return app

