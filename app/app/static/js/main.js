(function () {
    'use strict';

    const messageBox = document.getElementById('message-box');

    function displayMessage(message) {
        const now = new Date();
        const newNode = document.createElement('p');
        newNode.innerHTML = `${now.getHours()}:${now.getMinutes()}:${now.getSeconds()} -- ${JSON.stringify(message)}`;
        messageBox.prepend(document.createElement('hr'));
        messageBox.prepend(newNode);
    }

    document.getElementById('start_scraping').addEventListener('click', () => {
        fetch('/api/scrape')
            .then(response => response.json())
            .then(response => {
                displayMessage(response);
            })
            .catch(error => {
                displayMessage(error);
            });
    });

    document.getElementById('init_indices').addEventListener('click', () => {
        fetch('/api/initialize-indices')
            .then(response => response.json())
            .then(response => {
                displayMessage(response);
            })
            .catch(error => {
                displayMessage(error);
            });
    });

    document.getElementById('start_scraping_2010_2016').addEventListener('click', () => {
        fetch('/api/scrape-between-2010-and-2016')
            .then(response => response.json())
            .then(response => {
                displayMessage(response);
            })
            .catch(error => {
                displayMessage(error);
            });
    });

    document.getElementById('start_delta_scraping').addEventListener('click', () => {
        fetch('/api/run-delta-scraping')
            .then(response => response.json())
            .then(response => {
                displayMessage(response);
            })
            .catch(error => {
                displayMessage(error);
            });
    });
})();