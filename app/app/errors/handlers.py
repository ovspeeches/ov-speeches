import logging

from app.errors import blueprint
from flask import request

logger = logging.getLogger(__name__)


@blueprint.route('/healthCheck')
@blueprint.route('/healthcheck')
def health_check():
    return '{"success": true}'


@blueprint.route('/api/frontendError', methods=['POST', 'GET'])
def receive_frontend_errors():
    logger.info("Received a frontend error with data {}".format(request.get_data()))
    return '{"message": "Received logs"}'
