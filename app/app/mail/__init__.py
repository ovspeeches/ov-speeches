from app.mail.mail import send_mail
from app.mail.mail import notify_scraping_start
from app.mail.template import generate_scraping_start_email_as_html, generate_scraping_start_email_as_text
