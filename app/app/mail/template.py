def generate_scraping_start_email_as_html(time_of_start):
    return """\
    <html>
      <head></head>
      <body>
        <p>Scraping started at {}</p>
      </body>
    </html>
    """.format(time_of_start)


def generate_scraping_start_email_as_text(time_of_start):
    return "Scraping started at {}".format(time_of_start)
