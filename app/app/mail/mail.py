import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from os import getenv
from logging import getLogger
from datetime import datetime

from app.mail.template import generate_scraping_start_email_as_text, generate_scraping_start_email_as_html


logger = getLogger(__name__)


def send_mail(subject, html, text):
    message = MIMEMultipart('alternative')
    message['Subject'] = subject
    message['From'] = getenv('NOTIFIER_EMAIL')
    message['To'] = getenv('NOTIFICATION_TARGET_EMAIL')

    part1 = MIMEText(text, 'plain')
    part2 = MIMEText(html, 'html')

    message.attach(part1)
    message.attach(part2)

    try:
        mail = smtplib.SMTP(host='smtp.gmail.com', port=587, timeout=10)

        mail.ehlo()
        mail.starttls()

        mail.login(getenv('NOTIFIER_EMAIL'), getenv('NOTIFIER_EMAIL_PASSWORD'))
        mail.sendmail(getenv('NOTIFIER_EMAIL'), getenv('NOTIFICATION_TARGET_EMAIL'), message.as_string())
        mail.quit()
    except smtplib.SMTPAuthenticationError:
        logger.error("Incorrect username or password")
    except OSError:
        logger.error("Network is unreachable or request timed out")


def notify_scraping_start():
    now = datetime.now()
    send_mail('Scraping start', generate_scraping_start_email_as_html(now), generate_scraping_start_email_as_text(now))
