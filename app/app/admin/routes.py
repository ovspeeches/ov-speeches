import logging

from app.admin import blueprint
from app.models import admin
from flask import request, render_template, redirect, url_for
from flask_login import login_user, logout_user, login_required

logger = logging.getLogger(__name__)


@blueprint.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        if username == admin.name and password == admin.password:
            login_user(admin)
            logger.info('User {0} successfully logged in'.format(username))
            return redirect('/api/administration')
        else:
            logger.info('Failed login attempt with username {0} and password {1}'.format(username, password))
            return redirect('/api/login')
    return render_template('admin/login.html')


@blueprint.route('/logout')
@login_required
def logout():
    logout_user()
    return render_template('admin/login.html')


@blueprint.route('/administration')
@login_required
def index():
    return render_template('admin/administration.html')
