from flask import Blueprint

blueprint = Blueprint('elastic_proxy', __name__)

from app.elastic_proxy import routes