from flask import request, Response, jsonify
from flask_login import login_required
import requests
import logging
import json

from app.elastic_proxy import blueprint

logger = logging.getLogger(__name__)


@blueprint.route('/speeches,interviews/_doc/_search', methods=['POST'])
@blueprint.route('/_mget', methods=['POST'])
@blueprint.route('/speeches,interviews/_search', methods=['POST'])
def elasticsearch_proxy():
    logger.info("Elasticsearch proxy called with method {}, from host {}, with data {}"
                .format(request.method, request.url, request.get_data()))
    response = requests.request(
        method=request.method,
        url=request.url.replace(request.host_url, 'http://elasticsearch:9200/').replace('/api', ''),
        headers={key: value for (key, value) in request.headers if key != 'Host'},
        data=request.get_data(),
        cookies=request.cookies,
        allow_redirects=False
    )

    excluded_headers = ['content-encoding', 'content-length', 'transfer-encoding', 'connection']
    headers = [(name, value) for (name, value) in response.raw.headers.items() if name.lower() not in excluded_headers]

    return Response(response.content, response.status_code, headers)


@blueprint.route('/initialize-indices')
@login_required
def initialize_indices():
    response = requests.request(
        method='PUT',
        url='http://elasticsearch:9200/speeches',
        headers={'Content-type': 'application/json'},
        data=json.dumps({
            "settings": {
                "number_of_shards": 3,
                "number_of_replicas": 1
            },
            "mappings": {
                "_doc": {
                    "dynamic": "strict",
                    "properties": {
                        "title": {
                            "type": "text",
                            "index_phrases": "true",
                            "analyzer": "hungarian",
                            "boost": 1.5
                        },
                        "lead": {
                            "type": "text",
                            "index_phrases": "true",
                            "analyzer": "hungarian",
                            "boost": 1.25
                        },
                        "url": {"type": "text"},
                        "date": {"type": "date"},
                        "paragraphs": {
                            "type": "nested",
                            "dynamic": "strict",
                            "properties": {
                                "content": {
                                    "type": "text",
                                    "index_phrases": "true",
                                    "analyzer": "hungarian"
                                },
                                "interviewer_question": {"type": "boolean"}
                            }
                        }
                    }
                }
            }
        })
    )

    if response.status_code == 400 and \
            json.loads(response.content.decode()).get("error").get("type") == 'resource_already_exists_exception':
        speech_status = "Index already exists"
    elif response.status_code == 200:
        speech_status = "Index created"
    else:
        speech_status = "Something went wrong"

    response = requests.request(
        method='PUT',
        url='http://elasticsearch:9200/interviews',
        headers={'Content-type': 'application/json'},
        data=json.dumps({
            "settings": {
                "number_of_shards": 3,
                "number_of_replicas": 1
            },
            "mappings": {
                "_doc": {
                    "dynamic": "strict",
                    "properties": {
                        "title": {
                            "type": "text",
                            "index_phrases": "true",
                            "analyzer": "hungarian",
                            "boost": 1.5
                        },
                        "lead": {
                            "type": "text",
                            "index_phrases": "true",
                            "analyzer": "hungarian",
                            "boost": 1.25
                        },
                        "url": {"type": "text"},
                        "date": {"type": "date"},
                        "paragraphs": {
                            "type": "nested",
                            "dynamic": "strict",
                            "properties": {
                                "content": {
                                    "type": "text",
                                    "index_phrases": "true",
                                    "analyzer": "hungarian"
                                },
                                "interviewer_question": {"type": "boolean"}
                            }
                        }
                    }
                }
            }
        })
    )

    if response.status_code == 400 and \
            json.loads(response.content.decode()).get("error").get("type") == 'resource_already_exists_exception':
        interview_status = "Index already exists"
    elif response.status_code == 200:
        interview_status = "Index created"
    else:
        interview_status = "Something went wrong"

    return jsonify({'speech': speech_status, 'interview': interview_status})
