from app.models import User
from app import create_app
from flask import render_template
from flask import send_from_directory
import os

app = create_app()


@app.shell_context_processor
def make_shell_context():
    return {'User': User}


@app.errorhandler(404)
def fallback(error):
    return render_template('index.html')


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=80)
