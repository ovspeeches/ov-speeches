help:
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'


up:       ##      Runs application in development mode
	@docker-compose -f docker-compose.development.yml up --build

up-prod: ##       Runs application in production mode while building Docker images locally
	@docker-compose -f docker-compose.local-production-build.yml up --build

up-pull-prod: ##  Runs application by pulling images from Docker Hub
	@docker-compose -f docker-compose.production.yml up
