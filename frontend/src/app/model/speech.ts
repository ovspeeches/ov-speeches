export interface Speech {
  title: string;
  lead: string;
  url: string;
  paragraphs: SpeechParagraph[];
}

export interface SpeechParagraph {
  content: string;
  interviewer_question: boolean;
  isHighlighted: boolean;
}
