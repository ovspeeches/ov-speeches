import {Speech} from "./speech";

export interface ElasticResponse {
  _id: string;
  _index: string;
  _score: number;
  _source: Speech;
  _type: string;
  highlight: any;
  inner_hits: any;
  innerParagraphIndexes: number[];
}
