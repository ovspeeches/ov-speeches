import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {Speech} from '../../model/speech';
import {Subscription} from 'rxjs';
import * as Materialize from 'materialize-css';
import {SpeechRepositoryService} from '../../services/speech-repository.service';


@Component({
  templateUrl: './speech.component.html',
  styleUrls: ['./speech.component.css']
})
export class SpeechComponent implements OnInit, OnDestroy, AfterViewInit{

  speech: Speech = <Speech>{};
  id: string;
  queryParams;
  paramsSubscription: Subscription;
  isHighlightEnabled: boolean = true;

  @ViewChild('speechSection') speechSection: ElementRef;

  constructor(private speechRepository: SpeechRepositoryService, private route: ActivatedRoute){}

  ngOnInit(): void {
    this.paramsSubscription = this.route.params.subscribe((params: Params) => {
      this.id = params['speech-id'];
      this.speechRepository.getSpeechById(this.id).subscribe(speech => {
        this.speech = speech;
        this.route.queryParams.subscribe(queryParams => {
          this.queryParams = queryParams;

          let paragraphs = queryParams.highlightedParagraphs;
          if (!Array.isArray(queryParams.highlightedParagraphs)) {
            paragraphs = [queryParams.highlightedParagraphs];
          }

          paragraphs.forEach(paragraph => {
            if (Number.parseInt(paragraph) || paragraph == '0') {
              this.speech.paragraphs[Number.parseInt(paragraph)].isHighlighted = true;
            }
          });

        });
      });
    })
  }

  ngAfterViewInit(): void {
    const buttons = this.speechSection.nativeElement.querySelectorAll('.fixed-action-btn');
    Materialize.FloatingActionButton.init(buttons, {direction: 'top'});
    Materialize.Tooltip.init(buttons, {
      enterDelay: 500,
      exitDelay: 100,
    })
  }

  ngOnDestroy(): void {
    this.paramsSubscription.unsubscribe();
  }

  toggleHighlighting(): void {
    this.isHighlightEnabled = !this.isHighlightEnabled;
    this.speech.paragraphs.forEach(paragraph => {
      if (paragraph.hasOwnProperty('isHighlighted')) {
        paragraph.isHighlighted = !paragraph.isHighlighted;
      }
    });
  }

}
