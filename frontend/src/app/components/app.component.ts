import {Component} from '@angular/core';
import {NavigationEnd, Router} from "@angular/router";
import {filter} from "rxjs/operators";

declare var gtag;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {

  constructor(private router: Router) {
    const navigationEndEvents = router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    );

    navigationEndEvents.subscribe((event: NavigationEnd) => {
      gtag('config', 'UA-47784349-2', {'page_path': event.urlAfterRedirects});
    })
  }

}
