import {Component, OnDestroy, OnInit} from "@angular/core";
import {ElasticResponse} from "../../model/elastic-response";
import {ElasticClient} from "../../services/elastic.client";
import {ActivatedRoute, Params} from "@angular/router";
import {Subscription} from "rxjs/index";

@Component({
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.css']
})
export class SearchResultsComponent implements OnInit, OnDestroy {
  searchTerm: string = '';
  searchResults: ElasticResponse[] = [];
  paramsSubscription: Subscription;

  constructor(private elasticClient: ElasticClient, private route: ActivatedRoute){}

  ngOnInit(): void {
    this.paramsSubscription = this.route.queryParams.subscribe((queryParams: Params) => {

      this.searchTerm = queryParams['query'];
      this.initiateSearch();
    })
  }

  initiateSearch(): void {
    this.elasticClient.search(this.searchTerm).subscribe(results => {
      this.searchResults = results;
      this.searchResults.forEach(result => {
        result.innerParagraphIndexes = result.inner_hits.paragraphs.hits.hits.map(hit => hit._nested.offset);
        if (!result.highlight) {
          result.highlight = result.inner_hits.paragraphs.hits.hits.map(hit => hit._source.content);
        }
      });
      console.log(this.searchResults);
    });
  }

  ngOnDestroy(): void { this.paramsSubscription.unsubscribe(); }
}
