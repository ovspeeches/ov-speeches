import {Component, ViewEncapsulation} from '@angular/core';

@Component({
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class IndexComponent {

  constructor() { }
}
