import {AfterViewInit, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import * as Materialize from 'materialize-css';

@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements AfterViewInit, OnInit, OnDestroy{
  @ViewChild('navbar') navBar: ElementRef;
  @Input('solid') isSolid: boolean = false;

  constructor(private element: ElementRef){}

  ngAfterViewInit(): void {
    const sidenavs = this.element.nativeElement.querySelectorAll('.sidenav');
    Materialize.Sidenav.init(sidenavs, null);
  }

  ngOnInit(): void {
    if (!this.isSolid) { window.addEventListener('scroll', this.scroll, true); }
  }

  ngOnDestroy(): void {
    window.removeEventListener('scroll', this.scroll, true);
  }

  scroll = (): void => {
    if (window.scrollY > 600) {
      this.navBar.nativeElement.classList.remove('transparent');
    } else {
      this.navBar.nativeElement.classList.add('transparent');
    }
  }
}
