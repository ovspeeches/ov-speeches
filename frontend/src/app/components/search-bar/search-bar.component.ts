import {AfterViewInit, Component, ElementRef, ViewChild} from "@angular/core";
import {Router} from "@angular/router";

@Component({
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css'],
  selector: 'search-bar'
})
export class SearchBarComponent implements AfterViewInit {
  searchTerm: string = '';
  @ViewChild('searchBar') searchBar: ElementRef;

  constructor(private elementRef: ElementRef, private router: Router){}

  ngAfterViewInit(): void {
    this.searchBar.nativeElement.focus();
  }

  initiateSearch(event): void {
    if(!(event instanceof MouseEvent) && !(event instanceof KeyboardEvent && event.key === "Enter")) { return; }
    this.router.navigate(['search'], { queryParams: { query: this.searchTerm } });
  }

}
