import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';

import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {ElasticClient} from "./services/elastic.client";
import {AppComponent} from './components/app.component';
import {NavbarComponent} from './components/navbar/navbar.component';
import {RouterModule, Routes} from '@angular/router';
import {SpeechComponent} from './components/speech/speech.component';
import {IndexComponent} from './components/index/index.component';
import {AboutComponent} from './components/about/about.component';
import {SpeechRepositoryService} from './services/speech-repository.service';
import {ErrorsHandler} from './error/errors-handler';
import {ServerErrorsInterceptor} from './error/server-errors-interceptor';
import {NotificationService} from './services/notification.service';
import {ErrorsService} from './services/errors.service';
import {SearchResultsComponent} from "./components/search-results/search-results.component";
import {SearchBarComponent} from "./components/search-bar/search-bar.component";

const routes: Routes = [
  { path: '', component: IndexComponent },
  { path: 'search', component: SearchResultsComponent },
  { path: 'speech/:speech-id', component: SpeechComponent},
  { path: 'about', component: AboutComponent},
  { path: '*', component: IndexComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SpeechComponent,
    IndexComponent,
    AboutComponent,
    SearchResultsComponent,
    SearchBarComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    ElasticClient,
    ErrorsService,
    NotificationService,
    SpeechRepositoryService,
    {provide: ErrorHandler, useClass: ErrorsHandler},
    {provide: HTTP_INTERCEPTORS, useClass: ServerErrorsInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
