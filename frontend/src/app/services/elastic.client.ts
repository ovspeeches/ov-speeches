import {Injectable, OnInit} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {filter, map, mergeMap, tap} from "rxjs/operators";
import {Speech} from "../model/speech";
import {Observable} from "rxjs/internal/Observable";
import {ElasticResponse} from "../model/elastic-response";

@Injectable({
  providedIn: 'root'
})
export class ElasticClient implements OnInit{
  headers: HttpHeaders;

  constructor(private http: HttpClient){}

  ngOnInit(): void {
    this.headers = new HttpHeaders();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
  }

  search(searchTerm: string): Observable<ElasticResponse[]> {
    const url = '/api/speeches,interviews/_doc/_search';
    const requestBody = {
      "query": {
        "nested": {
          "path": "paragraphs",
          "query": {
            "match_phrase": {
              "paragraphs.content": searchTerm
            }
          },
          "inner_hits": {}
        }
      },
      "from": 0, "size": 20,
      "highlight": {
        "fragment_size": 2000,
        "fields": {
          "paragraphs.content": {}
        }
      }
    };
    return this.http.post(url, requestBody, {headers: this.headers})
      .pipe(
        map((response: any) => response.hits.hits),
        tap((documents: any[]) => documents.forEach(document => {
          localStorage.setItem(document._id, JSON.stringify(document._source));
        }))
      );
  }

  getSpeechById(id: string): Observable<Speech> {
    const url = '/api/_mget';
    const requestBody = {
      "docs": [
        {
          "_index" : "speeches",
          "_type" : "_doc",
          "_id" : id
        },
        {
          "_index" : "interviews",
          "_type" : "_doc",
          "_id" : id
        }
      ]
    };

    return this.http.post(url, requestBody, {headers: this.headers})
      .pipe(
        mergeMap((response: any) => response.docs),
        filter((document: any) => document.found),
        map((document: any) => document._source)
      );
  }

  getAllSpeeches(): Observable<Speech[]> {
    const url = '/api/speeches,interviews/_search';
    const requestBody = {
      "size": 500,
      "query": {"match_all": {}}
    };

    return this.http.post(url, requestBody, {headers: this.headers})
      .pipe(map((response: any) => response.hits.hits));
  }
}
