import {Injectable, Injector} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {LocationStrategy, PathLocationStrategy} from '@angular/common';

import * as StackTraceParser from 'error-stack-parser';

@Injectable()
export class ErrorsService {

  constructor(private injector: Injector, private router: Router, private http: HttpClient) { }

  log(error): void {
    console.error(error);
    const errorToSend = this.addContextInfo(error);
    this.http.post('/api/frontendError', errorToSend);
  }

  private addContextInfo(error) {
    const name = error.name || null;
    const time = new Date().getTime();
    const location = this.injector.get(LocationStrategy);
    const url = location instanceof PathLocationStrategy ? location.path() : '';
    const status = error.status || null;
    const message = error.message || error.toString();
    const stack = error instanceof HttpErrorResponse ? null : StackTraceParser.parse(error);

    return {name, time, location, url, status, message, stack};
  }

}
