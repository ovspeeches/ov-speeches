import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {Speech} from '../model/speech';
import {ElasticClient} from './elastic.client';
import {tap} from 'rxjs/operators';

@Injectable()
export class SpeechRepositoryService {

  constructor(private elasticClient: ElasticClient){}

  getSpeechById(id: string): Observable<Speech> {

    const localStorageItem = localStorage.getItem(id);

    if (localStorageItem) {
      return of(JSON.parse(localStorageItem));
    } else {
      return this.elasticClient.getSpeechById(id)
        .pipe(
          tap((document: any) => localStorage.setItem(id, JSON.stringify(document)))
        )
    }
  }
}
